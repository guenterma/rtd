#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from astropy.io import fits
import sunpy.map
import matplotlib.pyplot as plt
from sunpy.net.helioviewer import HelioviewerClient
import astropy.units as u
import astropy.io.fits as fitsio
import os
import sys
import numpy as np

class createPic():
   """
   :param sumfile: quelle der beobachtung
   :param savepath: Angebe des Speicherorts
    """

   def buildPrePic(self):
       """
       Creates a preview pic of the observation

       :param filename: The path to the original file
       :param test: test3
       """
       sumfile = os.path.join(sys.path[0], "lars_l12_20180428-170154_clv5576_mu085_n.ns.chvtt.sum.fits")
       headerList = fitsio.open(sumfile,ignore_missing_end=True)
       filename = os.path.basename(sumfile)
       dat = headerList[0].data
       plt.plot(dat[2,:]*10**10,dat[0,:])
       plt.xlabel('Wavelength [$\AA$]')
       plt.ylabel('Intensity [a.u.]')
       filename = filename.replace("chvtt.sum.fits","pre.png")
       plt.savefig(os.path.join(sys.path[0],filename), bbox_inches='tight',dpi=150)
       plt.close()

cp = createPic()
cp.buildPrePic()
